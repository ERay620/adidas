package com.adidas.runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(

        plugin = {"json:target/cucumber.json",
                "html:target/default-html-reports", "summary"},
        features = "src/tests/resources/features/",
        glue = "com/adidas/step_definitions/",
        monochrome = true,
        dryRun = false,
        tags = "@all"
)

public class CukesRunner {

}

/*
"@allPets"
"@addPets"
"@updatePets"
"@deletePets"
 */
