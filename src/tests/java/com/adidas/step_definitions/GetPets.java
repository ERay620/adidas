package com.adidas.step_definitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class GetPets extends StepBase {


    @Given("user has selected {string} status option")
    public void user_has_selected_status_option(String status) {
        this.status = status;
        System.out.println("When I selected status is: " + status);

    }

    @When("user has provided {string} as an endPoint")
    public void user_has_provided_as_an_endPoint(String endPoint) {
        this.endPoint = endPoint;
        System.out.println("And endPoint is: " + endPoint);

    }

    @Then("user should be able to get all matching  pets")
    public void user_should_be_able_to_get_all_matching_pets() {

        Response response = RestAssured.given().accept(ContentType.JSON)
                .queryParam("status", status)
                .and().when().get(baseURI + "/" + endPoint);

              assertEquals(200, response.statusCode());

        List<Map<String, Object>> pets = response.body().as(List.class);

        System.out.println("The total number of pets is: " + pets.size());

        for (int i = 0; i < pets.size(); i++) {

            System.out.println(i + "- " + pets.get(i));
            // System.out.println("Pet number " + i + ", status  = "+ pets.get(i).get("status"));

            assertEquals(pets.get(i).get("status"), "available");
        }



    }

}
