package com.adidas.step_definitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import java.util.Map;

import static io.restassured.RestAssured.given;


public class UpdatePets extends StepBase {



    @Given("user has selected a pet whose id {int}")
    public void user_has_selected_a_pet_whose_id(Integer id) {
        RestAssured.given().accept(ContentType.JSON)
                .queryParam("status", status)
                .and().when().get(baseURI + "/" + id)
                .then().assertThat().statusCode(200);

    }

    @When("user has updated this pet with the below values")
    public void user_has_updated_this_pet_with_the_below_values(Map<String, String> dataTable) {

        String id = dataTable.get("id");
        String name = dataTable.get("name");
        String status = dataTable.get("status");

        String pet = "{\n" +
                "  \"id\": " + id + ",\n" +
                "  \"category\": {\n" +
                "    \"id\": 0,\n" +
                "    \"name\": \"string\"\n" +
                "  },\n" +
                "  \"name\": \"" + name + "\",\n" +
                "  \"photoUrls\": [\n" +
                "    \"string\"\n" +
                "  ],\n" +
                "  \"tags\": [\n" +
                "    {\n" +
                "      \"id\": 0,\n" +
                "      \"name\": \"string\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"status\": \"" + status + "\" \n" +
                "}";

        System.out.println(pet);

                 given().contentType(ContentType.JSON)
                .and().body(pet)
                .when().put(baseURI )
                .then().assertThat().statusCode(200);
    }


    @Then("user should be able to get his own  pet whose id {int}")
    public void user_should_be_able_to_get_his_own_pet_whose_id(Integer id) {
                 given().accept(ContentType.JSON)
                .queryParam("status", status)
                .and().when().get(baseURI + "/" + 56)
                .then().assertThat().statusCode(200);

    }

}
