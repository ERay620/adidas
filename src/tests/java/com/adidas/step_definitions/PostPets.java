package com.adidas.step_definitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import java.util.Map;


public class PostPets extends StepBase {

    private String pet;

    @Given("user has selected the below options")
    public void user_has_selected_the_below_options(Map<String, String> dataTable) {
        String id = dataTable.get("id");
        String name = dataTable.get("name");
        String status = dataTable.get("status");

        pet = "{\n" +
                "  \"id\": " + id + ",\n" +
                "  \"category\": {\n" +
                "    \"id\": 0,\n" +
                "    \"name\": \"string\"\n" +
                "  },\n" +
                "  \"name\": \"" + name + "\",\n" +
                "  \"photoUrls\": [\n" +
                "    \"string\"\n" +
                "  ],\n" +
                "  \"tags\": [\n" +
                "    {\n" +
                "      \"id\": 0,\n" +
                "      \"name\": \"string\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"status\": \"" + status + "\" \n" +
                "}";

        System.out.println(pet);

    }

    @When("user has created a new pet")
    public void user_has_created_a_new_pet() {

        RestAssured.given().accept(ContentType.JSON)
                .and().contentType(ContentType.JSON)
                .and().body(pet)
                .when().post(baseURI).then().assertThat().statusCode(200);
    }

}
