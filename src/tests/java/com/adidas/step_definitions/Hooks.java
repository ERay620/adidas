package com.adidas.step_definitions;

import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;



public class Hooks {

    @Before
    public void setUp() {

        System.out.println("First of all, WELCOME to ADIDAS");
    }

    @After
    public void tearDown(Scenario scenario) {

        System.out.println("Finally, WELCOME to ADIDAS");
    }
}
