package com.adidas.step_definitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;

import static io.restassured.RestAssured.given;

public class DeletePets extends StepBase {

    @When("user has deleted this pet")
    public void user_has_deleted_this_pet() {

    }

    @When("user has deleted this pet whose id {int}")
    public void user_has_deleted_this_pet_whose_id(Integer id) {

        Response response = given().contentType(ContentType.JSON)
                .and().pathParam("petId", id)
                .when().delete(baseURI + "/{petId}");

        Assert.assertEquals(200, response.statusCode());

    }

    @Then("user should not be able to get this  pet whose id {int}")
    public void user_should_not_be_able_to_get_this_pet_whose_id(Integer id) {

        Response response = RestAssured.given().accept(ContentType.JSON)
                .queryParam("status", status)
                .and().when().get(baseURI + "/" + id);
        Assert.assertEquals(404, response.statusCode());

    }

}
