package com.adidas.step_definitions;

import com.adidas.utilities.ConfigurationReader;
import io.restassured.response.Response;

public class StepBase {

    protected String status;
    protected String baseURI = ConfigurationReader.get("url");
    protected String endPoint;

}
