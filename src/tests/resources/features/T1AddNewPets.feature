Feature: POST New  Pets

  As an authenticated
  I want to add new pets
  So that I should be see this pet later

  @addPets  @all
  Scenario: User add a specific  pet
    Given  user has selected the below options
      | id     | 56        |
      | status | available |
      | name   | name56    |
    When   user has created a new pet
    Then   user should be able to get his own  pet whose id 56
