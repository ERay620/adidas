Feature: GET All Pets

  As an authenticated
  I want to see all available pets
  So that I can select my pet base on their status

  @allPets   @all
  Scenario: Get all available pets
    Given user has selected "available" status option
    When  user has provided "findByStatus" as an endPoint
    Then  user should be able to get all matching  pets
