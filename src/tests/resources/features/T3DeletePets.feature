Feature: DELETE  Pets

  As an authenticated
  I want to delete my pets
  So that I should not be see this pet later

  @deletePets  @all
  Scenario: User delete a specific  pet
    Given  user has selected a pet whose id 56
    When   user has deleted this pet whose id 56
    Then   user should not be able to get this  pet whose id 56