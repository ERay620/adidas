Feature: PUT Pets

  As an authenticated
  I want to update my pets status
  So that I should be see this update

  @updatePets  @all
  Scenario: User update a specific  pet
    Given  user has selected a pet whose id 56
    When   user has updated this pet with the below values
      | id     | 56     |
      | status | sold   |
      | name   | name76 |
    Then   user should be able to get his own  pet whose id 56

